<?php
/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 4/19/16
 * Time: 11:22 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = array('title', 'content');
}